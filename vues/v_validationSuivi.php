<?php
/**
 * Vue de validation des frais et du suivit de paiement
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @author    Auriane DOPPLER <auriane.doppler@free.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>
<hr>
<div class="panel panel-primary">
    <div class="panel-heading">Fiche de frais du mois 
        <?php echo $numMois . '-' . $numAnnee . ' du visiteur ' . $nomVisiteur ?> : </div>
    <!-- message d'erreur si la fiche selectionnée n'existe pas -->
    <?php if (!$ficheExiste) { ?>
        <div class="panel-body" style="color: red; text-align: center;">
            <strong>Pas de fiche de frais pour ce visiteur ce mois</strong>
        </div>
    <?php } else { ?>
    <!-- si la fiche selectionnée existe, affiche l'etat et le montant validé -->
    <div class="panel-body">
        <strong><u>Etat :</u></strong> <?php echo $libEtat?>
        depuis le <?php echo $dateModif ?> <br> 
        <strong><u>Montant validé :</u></strong> <?php echo $montantValide ?>
    </div>
</div>

<!-- Affiche les éléments forfatisés et permet de les modifier si on est sur la vue "validation" -->
    <div class="panel panel-info">
        <div class="panel-heading">Eléments forfaitisés</div>
        <table class="table table-bordered table-responsive">
            <tr>
                <?php
                foreach ($lesFraisForfait as $unFraisForfait) {
                    $libelle = $unFraisForfait['libelle']; ?>
                    <th> <?php echo htmlspecialchars($libelle) ?></th>
                    <?php
                }
                ?>
            </tr>
            <form method="post" 
                action="index.php?uc=validerFrais&action=voirFiche&corrigeFF&visiteur=<?php echo $idVisiteur . '&mois=' . $leMois?>" 
                role="form">
                <tr>            
                    <?php
                    foreach ($lesFraisForfait as $unFraisForfait) {
                        $idFrais = $unFraisForfait['idfrais'];
                        $libelle = $unFraisForfait['libelle'];
                        $quantite = $unFraisForfait['quantite']; ?>
                    <div>
                        <?php if ($uc == 'validerFrais' && !$rembourse) { ?> 
                        <td class="qteForfait">
                            <input type="text" id="idFrais" 
                                    name="lesFrais[<?php echo $idFrais ?>]"
                                    size="10" maxlength="5" 
                                    value="<?php echo $quantite ?>" 
                                    class="form-control">                        
                        </td>
                        <?php } else if ($uc == 'suiviFrais' || $rembourse) { ?>
                            <td class="qteForfait"><?php echo $quantite ?> </td>
                        <?php } } ?>
                        <?php if ($uc == 'validerFrais' && !$rembourse ) { ?>
                        <td> <button class="btn btn-success" type="submit">Corriger</button></td>
                        <?php } ?>
                    </div>                 
                </tr>
            </form> 

            
        </table>
    </div>
<!-- Affiche les éléments hors forfait et permet de les modifier si on est sur la vue "validation" -->
    <div class="panel panel-info">
        <div class="panel-heading">Descriptif des éléments hors forfait </div>
        <table class="table table-bordered table-responsive">
            <tr>
                <th class="date">Date</th>
                <th class="libelle">Libellé</th>
                <th class='montant'>Montant</th> 
                <?php if ($uc == 'validerFrais' && !$rembourse) { ?>
                    <th class='corriger' width="10%" style="text-align: center"></th>  
                    <th class='supprimer' width="10%" style="text-align: center">Supprimer</th>   
                    <th class='reporter' width="10%" style="text-align: center">Reporter</th>
                <?php  } ?>                    
            </tr>
            <?php
            foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
                $date = $unFraisHorsForfait['date'];
                $libelle = htmlspecialchars($unFraisHorsForfait['libelle']);
                $montant = $unFraisHorsForfait['montant'];
                $id = $unFraisHorsForfait['id'] ?>
                <tr>
                    <?php if ($uc == 'validerFrais' && !$rembourse) { ?>
                        <form method="post" role="form"
                        action="index.php?uc=validerFrais&action=voirFiche&corrigeHF=<?php echo $id ?>&visiteur=<?php echo $idVisiteur . '&mois=' . $leMois?>" >
                            <td><input type="text" id="txtDateHF" name="dateHF" size="10"
                                   maxlength="10" value="<?php echo $date ?>"  class="form-control"> </td>                                    
                            <td><input type="text" id="txtLibHF" name="libelleHF" size="10"
                                   maxlength="50" value="<?php echo $libelle ?>"  class="form-control"> </td>
                            <td><input type="text" id="txtMontantHF" name="montantHF" size="10"
                                   maxlength="8" value="<?php echo $montant ?>"  class="form-control"> </td>
                            <td>  <!-- bouton de correction du frais HF --> 
                                <input id="btnCorriger" type="submit" value="Corriger" class="btn btn-success" role="button">
                            </td>
                        </form>
                            <td style="text-align: center">
                                <a class="btn btn-default" href="index.php?uc=validerFrais&action=voirFiche&suppr=
                                <?php echo $unFraisHorsForfait['id'] . '&visiteur=' . $idVisiteur . '&mois=' . $leMois ?>">
                                    <span class="glyphicon glyphicon-remove" style="color: #d9534f"></span>
                                </a> 
                            </td>
                            <td style="text-align: center"> 
                                <a class="btn btn-default" href="index.php?uc=validerFrais&action=voirFiche&report=
                                <?php echo $unFraisHorsForfait['id'] . '&visiteur=' . $idVisiteur . '&mois=' . $leMois ?>">
                                    <span class="glyphicon glyphicon-hourglass" style="color: #0066cc"></span>
                                </a> 
                            </td>
                        <?php } else if ($uc == 'suiviFrais' || $rembourse) { ?>   
                            <td><?php echo $date ?></td>
                            <td><?php echo $libelle?> </td>
                            <td><?php echo $montant ?></td>   
                        <?php  } ?>      
                    </tr>
                <?php  } ?>
        </table>
    </div>

<!-- boutons de validation de la fiche ou de mise en paiement / remboursement, 
    si elle n'est pas déjà remboursee -->
    <?php if (!$rembourse) { ?>
    <div style="text-align: center;" >
        <?php if ($uc == 'validerFrais') { ?>
            <form method="post" role="form"
                action="index.php?uc=validerFrais&action=voirFiche&valider&visiteur=<?php echo $idVisiteur . '&mois=' . $leMois ?>" >
                <div style="text-align: left;"> Nombre de Justificatifs:
                    <input type="text" id="justificatif" name="nbJustificatifs" size="10" maxlength="5"
                            style="width: 10%" value="<?php echo $nbJustificatifs ?>" class="form-control choix-fiche">
                </div>
                <input id="validation" type="submit" value="Valider la Fiche" 
                    style="margin-bottom: 25px;" role="button" class="btn btn-success">
            </form>
        <?php } else if ($uc == 'suiviFrais') { ?> 
        <a href="index.php?uc=suiviFrais&action=voirFiche&rembourse&visiteur=<?php echo $idVisiteur . '&mois=' . $leMois ?>" 
        class="btn btn-success" role='button' style="margin-bottom: 25px;"> 
            <span class="glyphicon glyphicon-check" style="color: white;"></span>
            Fiche Remboursée
        </a>
        <?php } ?> 
    </div>
    <?php } ?> 
<?php } ?>