<?php
/**
 * Vue de choix de la fiche à afficher pour validation des fiches et suivit du paiement:
 * Selection du visiteur et du mois voulus.
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @author    Auriane DOPPLER <auriane.doppler@free.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>


<h2> <?php if ($uc == 'validerFrais') {
    ?> Valider une fiche de frais 
<?php } else if ($uc == 'suiviFrais') {
    ?> Suivre le paiement d'une fiche 
<?php } ?> </h2>
<div class="row">
    <div class="col-md-4">
        <h3>Sélectionner une fiche : </h3>
    </div>
    <div class="col-md-4">
        <form <?php if ($uc == 'validerFrais') {
            ?> action="index.php?uc=validerFrais&action=voirFiche"  
            <?php } else if ($uc == 'suiviFrais') {
            ?> action="index.php?uc=suiviFrais&action=voirFiche" <?php } ?>            
            method="post" role="form">
            <!-- choix du visiteur à selectionner -->
            <div class="form-group">
                <label for="lstVisiteur" accesskey="n">Visiteur : </label>
                <select id="lstVisiteur" name="lstVisiteur" class="form-control choix-fiche">
                    <?php 
                    foreach ($lesVisiteurs as $unVisiteur) {
                        $id = $unVisiteur['id'];
                        $nom = $unVisiteur['nom'];
                        $prenom = $unVisiteur['prenom'];
                        if ($id == $visiteurASelectionner) {
                            ?>
                            <option selected value="<?php echo $id ?>">
                                <?php echo $nom . ' ' . $prenom ?> </option>
                            <?php
                        } else {
                            ?>
                            <option value="<?php echo $id ?>">
                                <?php echo $nom . ' ' . $prenom ?> </option>
                            <?php
                        }
                    }
                    ?>    
                </select>
                <!-- choix du mois en fonction du visiteur selectionné -->
                <label for="lstMois" accesskey="n">Mois : <?php echo sizeof($lesMois) ?></label>
                <select id="lstMois" name="lstMois" class="form-control choix-fiche">
                    <?php
                    foreach ($lesMois as $unMois) {
                        $mois = $unMois['mois'];
                        $numAnnee = $unMois['numAnnee'];
                        $numMois = $unMois['numMois'];
                        if ($mois == $moisASelectionner) {
                            ?>
                            <option selected value="<?php echo $mois ?>">
                                <?php echo $numMois . '/' . $numAnnee ?> </option>
                            <?php
                        } else {
                            ?>
                            <option value="<?php echo $mois ?>">
                                <?php echo $numMois . '/' . $numAnnee ?> </option>
                            <?php
                        }
                    }
                    ?>   

                </select>
                
            </div>
     
            <input id="ok" type="submit" value="Afficher" class="btn btn-success" 
                   role="button">
            <input id="annuler" type="reset" value="Effacer" class="btn btn-danger" 
                   role="button">
        </form>
    </div>
</div>

