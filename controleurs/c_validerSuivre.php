<?php
/**
 * Contrôleur de validation et de suivi du paiement de frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @author    Auriane DOPPLER <auriane.doppler@free.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */

$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
$uc = filter_input(INPUT_GET, 'uc', FILTER_SANITIZE_STRING);
switch ($action) {
case 'choixFiche':
    //selection d'un visiteur
    $lesVisiteurs = $pdo->getLesVisiteurs();
    $lesClesVis = array_keys($lesVisiteurs);
    $visiteurASelectionner = ($lesVisiteurs[$lesClesVis[0]])['id'];

    //selection d'un mois pour le visiteur selectionné
    $lesMois = $pdo-> getLesMois();
    $lesClesMois = array_keys($lesMois);
    $moisASelectionner = $lesClesMois[0];
    include 'vues/v_choixFiche.php';
    break;

case 'voirFiche':
    //recupération du visiteur
    if (isset($_POST["lstVisiteur"])) {
        $idVisiteur = filter_input(INPUT_POST, 'lstVisiteur', FILTER_SANITIZE_STRING);
    }
    else if (isset($_GET["visiteur"])) {
        $idVisiteur = filter_input(INPUT_GET, 'visiteur', FILTER_SANITIZE_STRING);
    }
    //recupération du mois
    if (isset($_POST["lstMois"])) {
        $leMois = filter_input(INPUT_POST, 'lstMois', FILTER_SANITIZE_STRING);
    }
    else if (isset($_GET["mois"])) {
        $leMois = filter_input(INPUT_GET, 'mois', FILTER_SANITIZE_STRING);
    }

    //vérifie si la fiche existe pour ce mois / visiteur (true si la fiche n'existe pas)
    $ficheExiste = !($pdo->estPremierFraisMois($idVisiteur, $leMois));

    //suivi du paiement des frais
    //passe la fiche à l'état 'Remboursée'    
    if (isset($_GET["rembourse"])) {
        $idVisiteur = filter_input(INPUT_GET, 'visiteur', FILTER_SANITIZE_STRING);
        $leMois = filter_input(INPUT_GET, 'mois', FILTER_SANITIZE_STRING);
        $pdo->majEtatFicheFrais($idVisiteur, $leMois, "RB");
    }

    //corrige les frais hors forfaits
    if (isset($_GET["corrigeHF"])) {
        $idHF = filter_input(INPUT_GET, 'corrigeHF', FILTER_SANITIZE_STRING);
        $newDate = filter_input(INPUT_POST, 'dateHF', FILTER_SANITIZE_STRING);
        $newLib = filter_input(INPUT_POST, 'libelleHF', FILTER_SANITIZE_STRING);
        $newMontant = filter_input(INPUT_POST, 'montantHF', FILTER_SANITIZE_STRING);

        $pdo->majFraisHorsForfait($idHF, $newDate, $newLib, $newMontant);

        /*valideInfosFrais($dateFrais, $libelle, $montant);
        if (nbErreurs() != 0) {
            include 'vues/v_erreurs.php';
        } else {

        }*/
    }

    //variables
    //selection du visiteur
    $lesVisiteurs = $pdo->getLesVisiteurs();
    //$lesCles = array_keys($lesVisiteurs);
    $visiteurASelectionner = $idVisiteur;
    //selection d'un mois pour le visiteur selectionné
    $lesMois = $pdo->getLesMois();
    //$lesCles = array_keys($lesMois);
    $moisASelectionner = $leMois;

    include 'vues/v_choixFiche.php';

    //supprime le frais Hors forfait dont l'id est recu
    $idFrais = filter_input(INPUT_GET, 'suppr', FILTER_SANITIZE_STRING);
    if ($idFrais !== null) {
        $pdo->supprimerFraisHorsForfait($idFrais);
    }
    
    if (isset($_GET["corrigeFF"])) {
        $lesFrais = filter_input(INPUT_POST, 'lesFrais', FILTER_DEFAULT, FILTER_FORCE_ARRAY);
        if (lesQteFraisValides($lesFrais)) {
            $pdo->majFraisForfait($idVisiteur, $leMois, $lesFrais);            
            //calcul du montant validé    
        } else {
            ajouterErreur('Les valeurs des frais doivent être numériques');
            include 'vues/v_erreurs.php';
        }
    }

    //$montantValide = ($pdo->getMontantValide($idVisiteur, $leMois))[0];
    
    $leVisiteur = $pdo->getLeVisiteur($idVisiteur);
    $nomVisiteur = ($leVisiteur['prenom'] . ' ' . $leVisiteur['nom']);     
    $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $leMois);
    $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $leMois);

    $numAnnee = substr($leMois, 0, 4);
    $numMois = substr($leMois, 4, 2);
    $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $leMois);
    $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];    
    $libEtat = $lesInfosFicheFrais['libEtat'];    
    $rembourse = ($lesInfosFicheFrais['idEtat'] == 'RB');
    $dateModif = dateAnglaisVersFrancais($lesInfosFicheFrais['dateModif']);
    $montantValide = $lesInfosFicheFrais['montantValide'];
    
    //cloture la fiche
    if (isset($_GET["valider"])) {
        //calcul montant total des frais forfatisés
        $montantValide = $pdo->calcMontantForfait($lesFraisForfait);
      
        //ajout des frais hors forfait au montant validé
        //$lesCles = array_keys($lesFraisHorsForfait);
        foreach ($lesFraisHorsForfait as $unFrais) {
            $montantValide += $unFrais['montant'];
        }
        $pdo->majMontantValide($idVisiteur, $leMois, $montantValide);
        //valide le nombre de justificatifs    
        $nbJustificatifs = filter_input(INPUT_POST, 'nbJustificatifs', FILTER_DEFAULT, FILTER_SANITIZE_STRING);
        $pdo->majNbJustificatifs($idVisiteur, $leMois, $nbJustificatifs);
        //cloture la fiche
        $pdo->majEtatFicheFrais($idVisiteur, $leMois, 'VA'); 
    }   
    


    if (isset($_GET["report"])) {
        $idFraisReport = filter_input(INPUT_GET, 'report', FILTER_SANITIZE_STRING);
        //$newMois=$leMois;
        $dernierMois=$pdo->dernierMoisSaisi($idVisiteur);
        if ($dernierMois == $leMois) {
            if ($numMois < 12) {
                $newMois = $leMois+1;
            }
            else if ($numMois == 12) {
                $newMois = ($numAnnee+1) . '01';
            }
            $pdo->creeNouvellesLignesFrais($idVisiteur, $newMois);
        }
        else {
            $newMois=$dernierMois;
        }

        $pdo->reporterFrais($idFraisReport, $newMois);
    }

    include 'vues/v_validationSuivi.php';
}

